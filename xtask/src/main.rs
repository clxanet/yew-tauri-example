use std::{io::Read, path::PathBuf};
use wasm_bindgen_cli_support::Bindgen;

const DBG_DIR: &str = concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/../target/wasm32-unknown-unknown/debug/"
);
const REL_DIR: &str = concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/../target/wasm32-unknown-unknown/release/"
);
const DIST_DIR: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/../target/dist/");

fn main() {
    let mut args = std::env::args();
    let exe = args.next().unwrap();
    let command = args.next().unwrap();
    let wasm = args.next().unwrap();

    match command.as_str() {
        "devel" => pack(true, &wasm, DBG_DIR, DBG_DIR),
        "pack" => pack(false, &wasm, REL_DIR, DIST_DIR),
        _ => {
            eprintln!("USAGE: {exe} devel <SPA_PACKAGE_NAME>");
            eprintln!("USAGE: {exe} pack  <SPA_PACKAGE_NAME>");
            panic!()
        }
    }
}

fn pack(debug: bool, wasm_name: &str, wasm_dir: &str, out_dir: &str) {
    let path = PathBuf::from(wasm_dir)
        .join(wasm_name)
        .with_extension("wasm");
    println!("Reading wasm: {}", path.to_str().unwrap());
    let mut bytes = vec![];
    std::fs::File::open(path)
        .unwrap()
        .read_to_end(&mut bytes)
        .unwrap();

    println!("Preparing Outputs...");
    let spa = pack_spa(wasm_name, bytes, debug).unwrap();

    println!("Create output directory...");
    let out_dir = PathBuf::from(out_dir);
    std::fs::create_dir_all(&out_dir).unwrap();

    println!("Writing index.html...");
    let (index_ante, index_post) = spa.index("/", "<title>Hello Tauri</title>");
    std::fs::write(
        out_dir.join("index.html"),
        vec![index_ante, index_post].join(""),
    )
    .unwrap();

    println!("Writing {wasm_name}.js...");
    std::fs::write(out_dir.join(spa.js_path), spa.js_str).unwrap();

    println!("Writing {wasm_name}_bg.wasm...");
    std::fs::write(out_dir.join(spa.wasm_path), spa.wasm_bytes).unwrap();
}

#[derive(Debug)]
pub struct Spa {
    pub wasm_path: String,
    pub wasm_bytes: Vec<u8>,

    pub js_path: String,
    pub js_str: String,

    name: String,
}

#[derive(Debug)]
pub struct SpaError {
    msg: String,
}

fn pack_spa(name: &str, bytes: Vec<u8>, debug: bool) -> Result<Spa, SpaError> {
    let mut output = Bindgen::new()
        .input_bytes(name, bytes)
        .web(true)
        .unwrap() // UNWRAP: failure only when multiple targets are specified
        .debug(debug)
        .keep_lld_exports(debug)
        .keep_debug(debug)
        .remove_name_section(!debug)
        .remove_producers_section(!debug)
        .generate_output()
        .map_err(|e| e.to_string())?;

    let wasm_path = format!("{name}_bg.wasm");
    let wasm_bytes = output.wasm_mut().emit_wasm();

    let js_path = format!("{name}.js");
    let js_str = output.js().to_string();

    Ok(Spa {
        wasm_path,
        wasm_bytes,
        js_path,
        js_str,
        name: name.to_string(),
    })
}

impl Spa {
    pub fn index(&self, url_prefix: &str, head: &str) -> (String, String) {
        let name = &self.name;
        let first = format!("<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">{head}<link rel=\"preload\" href=\"{url_prefix}{name}_bg.wasm\" as=\"fetch\" type=\"application/wasm\" crossorigin=\"\"><link rel=\"modulepreload\" href=\"{url_prefix}{name}.js\"></head><body>");
        let second = format!("<script type=\"module\">import init from '{url_prefix}{name}.js';init('{url_prefix}{name}_bg.wasm');</script></body></html>");
        (first, second)
    }
}

impl From<String> for SpaError {
    fn from(msg: String) -> Self {
        Self { msg }
    }
}

impl std::fmt::Display for SpaError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl std::error::Error for SpaError {}
