## Development

install the tauri CLI

```
cargo install tauri-cli
```


debug builds:

```
cargo tauri dev
```

release builds:

```
cargo tauri build
```

You probably want to take control of the right-click menu... Add the javacsript equivalent of

```
 document.addEventListener('contextmenu', event => event.preventDefault());
 ```
